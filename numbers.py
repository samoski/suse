#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Though i didn't want to but i got a little(ALOT) of help online,
# reading scripts online.
# That feeling of inadequacy(skills) is what am getting right now, i need guidance
# and mentoring, i know i can be really good(GREAT) at this.
# I really don't want to giveup, i REALLY love coding, i no i need to practice
# and keep on practicing
# With programming, i have learnt to be patient and think logically

# {1001', '1000001', '1234'}


def spell(num, join=True):
    """
    List number of words
    """
    thousands = ['', 'thousand', 'million']
    tens = ['', 'ten', 'twenty', 'thirty', 'forty',
            'fifty', 'sixty', 'seventy', 'eighty', 'ninety']
    teens = ['', 'eleven', 'twelve', 'thirteen', 'fourteen',
             'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen']
    units = ['', 'one', 'two', 'three', 'four',
             'five', 'six', 'seven', 'eight', 'nine']

    # Empty list for number words
    words = []

    # Zero case
    if words == 0:
        words.append('zero')

    # Handling negative numbers
    if words < 0:
        words.append('negative')
        num = abs(num)

    # Processing nums and turning them to words
    numStr = '%d' % num       # int
    numStrLen = len(numStr)

    # This is where it gets tricky
    groups = (numStrLen + 2) / 3
    numStr = numStr.zfill(groups * 3)
    for i in range(0, groups * 3, 3):
        h, t, u = int(numStr[i]), int(numStr[i + 1]), int(numStr[1 + 2])
        g = groups - (i / 3 + 1)
        if h >= 1:
            words.append(units[h])
            words.append('hundred')
        if t > 1:
            words.append(tens[t])
        if u >= 1:
            words.append(units[u])
        elif t == 1:
            if u >= 1:
                words.append(teens[u])
            else:
                words.append(tens[t])
        else:
            if u >= 1:
                words.append(units[u])
        if (g >= 1) and ((h + t + u) > 0):
            words.append(thousands[g] + ',')
            if (g >= 1) and ((h + t + u) > 0):
                words.append(thousands[g] + ',')

                # joining parts
        if join:
            return ' '.join(words)
        return words


print spell(1234)
